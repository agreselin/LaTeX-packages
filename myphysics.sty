%% Reminder: \newcommand* (the starred version) provides better error-checking but cannot contain paragraph breaks. https://tex.stackexchange.com/questions/1050/whats-the-difference-between-newcommand-and-newcommand

%%%% Identification

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{myphysics}[2018/03/10 v0.1 Gathers code related to chemistry and physics]

%%%% Initial code

\RequirePackage{etoolbox}
\ifdef{\if@sans}
{}
{
  \newif\if@sans
  \@sansfalse
}

%%%% Declaration and execution of options

\DeclareOption{debug}
{
  \if@sans
    \PassOptionsToPackage{debug}{maybeitsfmath}
  \fi
}

\ProcessOptions\relax

%%%% Package loading

\RequirePackage{mathtools}
\RequirePackage{chemmacros}
\if@sans
  \RequirePackage{maybeitsfmath}
\else
  \RequirePackage{maybemath} % It also loads bm
\fi
\RequirePackage{mymacros} % For \textnormalf and possibly others

%%%% Main code

%%% Physical quantities

%% The symbol that represents a physical quantity is constituted by two parts: an identifier for the quantity (usually a letter) and an optional label (a subscript).
%% The macros \PhysQty@Identifier{}, \PhysQty@Label{} and \PhysQty@Around{} specify the commands that act on the identifier, the label, and the symbol as a whole, respectively.
\newcommand{\PhysQty@Around}[1]{\ensuremath{#1}}
\if@sans
  \newcommand{\PhysQty@Identifier}[1]{\textit{\maybeitsf{#1}}}
\else
  \newcommand{\PhysQty@Identifier}[1]{\maybebm{#1}}
\fi
\newcommand{\PhysQty@Label}[1]{\textnormalf{#1}}

%% Usage: \PhysicalQuantity{<quantity>}[<label>]
%% The <label> is printed as subscript in normal text, except the font family (serif or sans serif) is retained from the surrounding text.
%% The definition of \PhysicalQuantity is different depending on whether the second argument (the subscript) is given.
%% In case it's not, the command doesn't start a subscript (it uses a definition without "_" in it), thus avoiding the "Double subscript" error if used as, e.g., $\PhysicalQuantity{shubidu}_i$

%% Definition with \ifstrempty (it requires etoolbox)
%% See https://tex.stackexchange.com/questions/308/different-command-definitions-with-and-without-optional-argument/63041#63041
\NewDocumentCommand{\PhysicalQuantity}{m O{}}{%
  \ifstrempty{#2}{%
    \PhysQty@Around{\PhysQty@Identifier{#1}}%
  }{%
    \PhysQty@Around{\PhysQty@Identifier{#1}_\PhysQty@Label{#2}}%
  }%
}

%% Definition with \IfValueTF (which requires xparse)
%% \IfValueTF doesn't really check if the argument is empty, it checks whether it is the special -NoValue- marker.
%% This implies that if I define a macro like \newcommand{\greatness}{\PhysicalQuantity[1][]{G}}, '\greatness' will run the "true" branch of \IfValueTF, because the second argument is empty, not -NoValue-. This, in turn, means that $\greatness_i$ will give the "Double subscript" error.
%% Therefore, with this definition I'd need to define all that kind of macros in the LaTeX3 way (with \NewDocumentCommand or related commands), using the type of optional argument 'o'.
%% See xparse.pdf
%%     https://tex.stackexchange.com/questions/308/different-command-definitions-with-and-without-optional-argument/2925#2925
% \NewDocumentCommand{\PhysicalQuantity}{m o}{%
%   \IfValueTF {#2} {%
%     \PhysQty@Around{\PhysQty@Identifier{#1}_\PhysQty@Label{#2}}%
%   }{%
%     \PhysQty@Around{\PhysQty@Identifier{#1}}%
%   }%
% }

%% Spectroscopy and polarimetry
\newcommand*{\wvfreq}[1][]{\PhysicalQuantity{\nu}[#1]} %       Frequency
\newcommand*{\wvlength}[1][]{\PhysicalQuantity{\lambda}[#1]} % Wavelength
\newcommand*{\wvnr}[1][]{\PhysicalQuantity{\tilde{\nu}}[#1]} % Wave number
\newcommand*{\mec}[1][]{\PhysicalQuantity{\varepsilon}[#1]} %  Molar extinction coefficient
\NewDocumentCommand{\alphaD}{O{D} O{}}{\PhysQty@Around{[\alpha]_\PhysQty@Label{#1}^\PhysQty@Label{#2}}} % Specific rotation

%% Equilibrium constants
\NewChemEqConstant{\Keq}{K-equilibrium}{\PhysQty@Label{eq}}
\NewChemEqConstant{\Kf}{K-formation}{\PhysQty@Label{f}}
\NewChemEqConstant{\Kow}{K-octanol-water}{\PhysQty@Label{ow}}
\newcommand*{\eqconst}[1][]{\PhysicalQuantity{K}[#1]}
%% Use my definitions, not chemmacro's, if the typeface is sans serif
\if@sans
\AtBeginDocument
{
  \def\Keq{\eqconst[eq]}
  \def\Ka{\eqconst[a]}
  \def\Kb{\eqconst[b]}
  \def\Kw{\eqconst[w]}
  \def\Kf{\eqconst[f]}
  \def\Kow{\eqconst[ow]}
}
\fi

%% p functions
%% Use my definitions, not chemmacro's, if the typeface is sans serif
\if@sans
\AtBeginDocument
{
  \newcommand*{\pfunct}[1][]{\PhysQty@Around{\mathop{}\!\mathrm{\PhysQty@Identifier{\textnormalf{p}}}\mkern1mu}#1} % About '\mathop{}\!', see https://tex.stackexchange.com/questions/60545/should-i-mathrm-the-d-in-my-integrals/95681#95681
  \def\pH{\pfunct[\ch{H}]}
  \def\pOH{\pfunct[\ch{OH}]}
  \def\pKa{\pfunct[\Ka]}
  \def\pKb{\pfunct[\Kb]}
  \def\pKw{\pfunct[\Kw]}
}
\fi

%% Other physical quantities
\newcommand*{\Temp}[1][]{\PhysicalQuantity{T}[#1]} %           Temperature
\newcommand*{\ttime}[1][]{\PhysicalQuantity{t}[#1]} %          Time. \time is already defined, I don't know where.
\newcommand*{\Press}[1][]{\PhysicalQuantity{P}[#1]} %          Pressure
\newcommand*{\Vol}[1][]{\PhysicalQuantity{V}[#1]} %            Volume
\newcommand*{\mass}[1][]{\PhysicalQuantity{m}[#1]} %           Mass
\newcommand*{\Mass}[1][r]{\PhysicalQuantity{M}[#1]} %          Molar mass
\newcommand*{\amt}[1][]{\PhysicalQuantity{n}[#1]} %            Amount of substance
\newcommand{\NA}{\PhysicalQuantity{N}[A]} %                    Avogadro's number
\newcommand{\kB}{\PhysicalQuantity{k}[B]} %                    Boltzmann constant
\newcommand*{\Gibbs}[1][]{\PhysicalQuantity{G}[#1]} %          Gibbs energy
\newcommand*{\Hent}[1][]{\PhysicalQuantity{H}[#1]}  %          Enthalpy
\newcommand*{\Sentr}[1][]{\PhysicalQuantity{S}[#1]} %          Entropy
\newcommand*{\Uint}[1][]{\PhysicalQuantity{U}[#1]} %           Internal energy
\newcommand*{\Work}[1][]{\PhysicalQuantity{W}[#1]} %           Work
\newcommand*{\DD}[2][]{\Delta_{\PhysQty@Label{#1}}#2} % Difference between two values of the same quantity

%%% Chemistry

\newcommand{\UVvis}{UV\textendash{}Vis}
\newcommand{\MSESI}{\hyphennobreak{MS}ESI}
\AtBeginDocument % \HNMR and \CNMR are defined by bpchem too.
{
  \def\HNMR{\ch{^1H}~NMR}
  \def\CNMR{\ch{^{13}C}~NMR}
}
\newcommand{\HHCOSY}{\ch{^1H}\textendash{}\ch{^1H}~COSY}
\newcommand{\HCCOSY}{\ch{^1H}\textendash{}\ch{^{13}C}~COSY}
\newcommand{\HCHSQC}{\ch{^1H}\textendash{}\ch{^{13}C}~HSQC}
\newcommand{\DEPTHSQC}{\hyphennobreak{DEPT}HSQC}
\newcommand{\Buchner}{Büchner}
\newcommand{\VdW}{Van~der~Waals}
\newcommand{\vdW}{van~der~Waals}
\NewDocumentCommand{\Schoenflies}{m O{} O{}}{\ensuremath{#1_{#2\mathrm{#3}}}} %     Use it as in \Schoenflies{C}[n][v], \Schoenflies{C}[][s].
\NewDocumentCommand{\Mlknsymb}{m O{}}{\ensuremath{\mathrm{#1_{#2}}}} %              Use it as in \Mlknsymb{T}[2g].
\NewDocumentCommand{\termsymb}{m m O{} O{}}{\ensuremath{\mathrm{^{#1}#2_{#3}#4}}} % Use it as in \termsymb{2S+1}{L}[J] or \termsymb{4}{T}[2g][(F)].
\NewDocumentCommand{\ecfg}{m}{\ensuremath{\mathrm{#1}}} %                           E.g. \ecfg{1s^2 2s^1}

%% Descriptors
%% Most descriptors are defined by chemmacros, and I use those.
%% Note: "iso" and "neo" go upright, right before the name: isobutyl, neopentyl.
%% See also IUPAC, basic terminology of stereochemistry - https://www.iupac.org/publications/pac/1996/pdf/6812x2193.pdf
%%          the command \IUPAC, bpchem.pdf, p.2.
\newcommand{\desc}[1]{\emph{#1}}
\newcommand{\Pdesc}{(\desc{P})} % Housecroft, p. 658
\newcommand{\Mdesc}{(\desc{M})} % Housecroft, p. 658
\newcommand{\ndesc}{\desc{n}}
